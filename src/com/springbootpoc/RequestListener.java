package com.springbootpoc;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class RequestListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("clear data");
		
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("add  data");
		
	}

}
