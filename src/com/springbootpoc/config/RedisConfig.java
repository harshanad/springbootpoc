package com.springbootpoc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.GenericToStringSerializer;

import com.springbootpoc.redis.pubsub.MessagePublisher;
import com.springbootpoc.redis.pubsub.RedisMessagePublisher;

@Configuration
public class RedisConfig {

	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		return new JedisConnectionFactory();
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		template.setConnectionFactory(jedisConnectionFactory());
		template.setValueSerializer(new GenericToStringSerializer<Object>(Object.class));
		return template;
	}

//	@Bean
//	MessageListenerAdapter messageListener() {
//		return new MessageListenerAdapter(new RedisMessageSubscriber());
//	}

//	@Bean
//	MessageListenerAdapter messageListener2() {
//		return new MessageListenerAdapter(new RedisMessageSubscriber2());
//	}

	@Bean
	RedisMessageListenerContainer redisContainer() {
		final RedisMessageListenerContainer container = new RedisMessageListenerContainer();
		container.setConnectionFactory(jedisConnectionFactory());
		//container.addMessageListener(messageListener(), topic());
		//container.addMessageListener(messageListener(), topic2());
		//container.addMessageListener(messageListener2(), topic3());
		return container;
	}

	@Bean
	MessagePublisher redisPublisher() {
		return new RedisMessagePublisher(redisTemplate());
	}

	@Bean
	ChannelTopic topic() {
		return new ChannelTopic("pubsub:queue");
	}

	@Bean
	ChannelTopic topic2() {
		return new ChannelTopic("pubsub:queue2");
	}

	@Bean
	ChannelTopic topic3() {
		return new ChannelTopic("pubsub:queue3");
	}
}
