package com.springbootpoc.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.springbootpoc.model.Student;
import com.springbootpoc.service.StudentService;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping(value = "/api")
public class StudentController {

	@Autowired
	StudentService service;

	@RequestMapping(value = "/findstudent/{id}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	// @PreAuthorize("hasAuthority('instructor')")
	public HttpEntity<Student> findStudent(@PathVariable("id") String id)
			throws ClientProtocolException, IOException {
		/*
		 * if (id.contains("abc")) { throw new
		 * InvalidPropertyException(StudentController.class, "id", "invalid id"
		 * ); }
		 */
		System.out.println("throwing bad cred excep for find student..");
		throw new BadCredentialsException("bad cred excep");
	//	Student student = service.findStudent(id);
	//	return new ResponseEntity<>(student, HttpStatus.OK);
	}

	@RequestMapping(value = "/student", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public HttpEntity<Student> createStudent(@RequestBody Student student) {
		System.out.println("creating student..");
		Student studentSave = service.saveStudent(student);
		return new ResponseEntity<>(studentSave, HttpStatus.OK);
	}

	@RequestMapping(value = "/findstudent1/{id}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public HttpEntity<Student> findStudent1(@PathVariable("id") String id) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> requestEntity = new HttpEntity<>("", headers);
		ResponseEntity<JSONObject> responseEntity = rest.exchange(
				"http://localhost:8080?name=abc", HttpMethod.GET, requestEntity,
				JSONObject.class);
		System.out.println(responseEntity);
		return null;
	}

	@RequestMapping(value = "/findstudentdynamic", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public HttpEntity<List<Student>> findStudentDynamicFilter(
			@RequestParam Map<String, String> filteringMap)
			throws ClientProtocolException, IOException {
		List<Student> studentList = service
				.findStudentDynamicFilter(filteringMap);
		return new ResponseEntity<>(studentList, HttpStatus.OK);
	}
}