package com.springbootpoc.exception.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.InvalidPropertyException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.springbootpoc.dto.ExceptionMessage;

@ControllerAdvice
public class StudentExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(InvalidPropertyException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ExceptionMessage invalidPropertyException(final HttpServletRequest req, final InvalidPropertyException ex) {
		return new ExceptionMessage(ex.getPropertyName(), ex.getMessage());
	}

}
