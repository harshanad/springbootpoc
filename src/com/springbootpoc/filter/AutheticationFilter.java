package com.springbootpoc.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.springbootpoc.dto.ApplicationAuthenticationToken;

public class AutheticationFilter extends AbstractAuthenticationProcessingFilter {

	public AutheticationFilter() {
		super("/");
	}

	@Override
	public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
			final FilterChain chain) throws IOException, ServletException {

		final HttpServletRequest request = (HttpServletRequest) servletRequest;
		final HttpServletResponse response = (HttpServletResponse) servletResponse;
		if (!request.getMethod().equalsIgnoreCase("OPTIONS")) {
			Authentication authentication;
			try {
				authentication = attemptAuthentication(request, response);
			} catch (AuthenticationException e) {
				unsuccessfulAuthentication(request, response, e);
				return;
			}
			successfulAuthentication(request, response, chain, authentication);
		}
		chain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest paramHttpServletRequest,
			HttpServletResponse paramHttpServletResponse)
			throws AuthenticationException, IOException, ServletException {

		// Validate the token
		boolean isValidToken = true;
		ApplicationAuthenticationToken applicationAuthenticationToken = null;

		if (isValidToken) {
			// Go to authorization
			applicationAuthenticationToken = new ApplicationAuthenticationToken(checkAuthorization());
			applicationAuthenticationToken.setAuthenticated(true);
		} else {
			throw new BadCredentialsException("Invalid token");
		}

		return applicationAuthenticationToken;
	}

	private List<GrantedAuthority> checkAuthorization() {

		// Role should fetch from relevant api's for a given user
		String role = "instructor";

		final GrantedAuthority authority = new SimpleGrantedAuthority(role);
		final List<GrantedAuthority> grantedAuthority = new ArrayList<>();
		grantedAuthority.add(authority);

		return grantedAuthority;
	}

	@Override
	protected void unsuccessfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException failed) throws IOException, ServletException {
		SecurityContextHolder.clearContext();
		getFailureHandler().onAuthenticationFailure(request, response, failed);
	}

	@Override
	protected void successfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain chain, final Authentication authResult) throws IOException, ServletException {
		System.out.println("successfully authenticated");
		SecurityContextHolder.getContext().setAuthentication(authResult);
	}

}