package com.springbootpoc.model;

public class Assignment {
	
	String assignId;
	String grade;
	public String getAssignId() {
		return assignId;
	}
	public String getGrade() {
		return grade;
	}
	public void setAssignId(String assignId) {
		this.assignId = assignId;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
}
