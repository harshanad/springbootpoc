package com.springbootpoc.redis.pubsub;

import com.springbootpoc.model.Student;

public interface MessagePublisher {

    void publish(final String message);
    void publish(final Student message);
}
