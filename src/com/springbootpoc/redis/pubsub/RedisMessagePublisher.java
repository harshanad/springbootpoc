package com.springbootpoc.redis.pubsub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Service;

import com.springbootpoc.model.Student;


@Service
public class RedisMessagePublisher implements MessagePublisher {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private ChannelTopic topic;
    @Autowired
    private ChannelTopic topic2;
    @Autowired
    private ChannelTopic topic3;

    public RedisMessagePublisher() {
    }

    public RedisMessagePublisher(final RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void publish(final String message) {
        redisTemplate.convertAndSend(topic2.getTopic(), message);
    }
    
    public void publish(final Student message) {
    	redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<Student>(Student.class));
    	redisTemplate.convertAndSend(topic.getTopic(), message);
        redisTemplate.convertAndSend(topic2.getTopic(), message);
		redisTemplate.convertAndSend(topic3.getTopic(), message);
    }
}
