//package com.springbootpoc.redis.pubsub;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.data.redis.connection.Message;
//import org.springframework.data.redis.connection.MessageListener;
//import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
//import org.springframework.data.redis.serializer.RedisSerializer;
//import org.springframework.stereotype.Service;
//
//import com.springbootpoc.model.Student;
//
//@Service
//public class RedisMessageSubscriber implements MessageListener {
//
//	public static List<String> messageList = new ArrayList<String>();
//	RedisSerializer<Object> redisSerializer = new Jackson2JsonRedisSerializer(Student.class);
//
//	public void onMessage(final Message message, final byte[] pattern) {
//		messageList.add(message.toString());
//		Student student = (Student) redisSerializer.deserialize(message.getBody());
//		System.out.println("Message received. Student name: " + student.getName());
//	}
//}