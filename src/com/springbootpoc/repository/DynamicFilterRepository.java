package com.springbootpoc.repository;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.springbootpoc.model.Student;

@Repository
public class DynamicFilterRepository {

	@Autowired
	MongoTemplate mongoTemplate;

	public List<Student> listStudentByDynamicFiler(Map<String, String> filter) {

		Criteria criteria = new Criteria();

		if (filter != null && filter.size() > 0) {
			filter.forEach((key, value) -> {
				criteria.and(key).is(value);
			});
		}

		Query query = new Query(criteria);
		return mongoTemplate.find(query, Student.class);

	}

}
