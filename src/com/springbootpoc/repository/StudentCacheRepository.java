package com.springbootpoc.repository;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.springbootpoc.model.Student;

import redis.clients.jedis.util.Hashing;

@Component
@ConditionalOnProperty("redis.enroll.enabled")
@CacheConfig(cacheNames = "enroll", cacheManager = "springtoolCM")
public class StudentCacheRepository {

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Cacheable
	public Student findByKey(final String key) {
		final HashOperations<String, String, String> hashOps = this.stringRedisTemplate.opsForHash();
		final String hashKey = String.valueOf(Hashing.MURMUR_HASH.hash(key));
		final String enrollmentJson = hashOps.get(key, hashKey);
		final Student enrollment = convertStudent(enrollmentJson);
		return enrollment;
	}

	public void setStudent(final String key, final Student enrollment) {
		final String hashKey = String.valueOf(Hashing.MURMUR_HASH.hash(key));
		this.stringRedisTemplate.expire(key, 30, TimeUnit.MINUTES);
		final HashOperations<String, String, String> hashOps = this.stringRedisTemplate.opsForHash();
		if (!this.stringRedisTemplate.hasKey(key)) {
			final String json = convertStudnet(enrollment);
			hashOps.put(key, hashKey, json);
		}
	}

	public void clearStudnet(final String key) {
		final String hashKey = String.valueOf(Hashing.MURMUR_HASH.hash(key));
		final HashOperations<String, String, String> hashOps = this.stringRedisTemplate.opsForHash();
		if (this.stringRedisTemplate.hasKey(key)) {
			hashOps.delete(key, hashKey);
		}
	}

	public String convertStudnet(final Student student) {
		final Gson gson = new Gson();
		return gson.toJson(student);
	}

	public Student convertStudent(final String student) {
		final Gson gson = new Gson();
		return gson.fromJson(student, Student.class);
	}
}