package com.springbootpoc.repository;

import org.springframework.data.repository.CrudRepository;

import com.springbootpoc.model.Student;

public interface StudentRepository extends CrudRepository<Student, String> {

}
