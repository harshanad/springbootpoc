package com.springbootpoc.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.springbootpoc.model.Student;

public interface StudentService {
        
    public Optional<Student> findStudent(String id);
    
    public Student saveStudent(Student student);
    
    public List<Student> findStudentDynamicFilter(Map<String, String> filter);

}
