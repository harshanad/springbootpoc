package com.springbootpoc.serviceimpl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springbootpoc.model.Student;
import com.springbootpoc.redis.pubsub.RedisMessagePublisher;
import com.springbootpoc.repository.DynamicFilterRepository;
import com.springbootpoc.repository.StudentCacheRepository;
import com.springbootpoc.repository.StudentRepository;
import com.springbootpoc.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository repository;

	@Autowired
	DynamicFilterRepository dynamicFilterRepository;

	@Autowired(required = false)
	StudentCacheRepository studentCacheRepository;

	@Autowired
	RedisMessagePublisher redisMessagePublisher;

	public  Optional<Student> findStudent(String id) {
		return repository.findById(id);
	}

	public Student saveStudent(Student student) {
		// studentCacheRepository.setStudent(student.getId(), student);
		// redisMessagePublisher.publish(student);
		return repository.save(student);
	}

	public List<Student> findStudentDynamicFilter(Map<String, String> filter) {

		return dynamicFilterRepository.listStudentByDynamicFiler(filter);
	}

}